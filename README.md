# **Angular Elements**

The project creates a simple, `<hello-world></hello-world>`, custom element as shown in Figure 1.

**Figure 1:**
![hello-world](./assets/hello-world.png)

## Development server

Run `ng serve -o` to test your angular component.

## **Build the Angular Element**

Run `npm run build:elements` script to build the angular app in production mode.

`build:elements` script will use the directions provided in **element-build.js** file to build a single hello-world.js file and it will move the file into elements folder. (Figure 2)

### **element-build.js**

```js
const fs = require('fs-extra');
const concat = require('concat');

(async function build() {
  const files = [
    './dist/angular-elements/runtime.js',
    './dist/angular-elements/polyfills.js',
    './dist/angular-elements/scripts.js',
    './dist/angular-elements/main.js'
  ];

  await fs.ensureDir('elements');
  await concat(files, 'elements/hello-world.js');
})();
```

**Figure 2:**

![elements-folder](./assets/elements-folder.png)

## **Use your custom element:**

Add a script source to point to hello-world.js file in your project.

For browsers like Chrome and Firefox, you may also need to add following script file to your project, so Custom Element can be rendered without any error messages.

```html
<script src="https://unpkg.com/@webcomponents/webcomponentsjs@2.1.3/custom-elements-es5-adapter.js"></script>
```
