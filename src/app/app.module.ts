import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Injector } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { createElement } from '@angular/core/src/view/element';
import { ReactiveFormsModule } from '@angular/forms';
import { HelloComponent } from './hello/hello.component';

@NgModule({
  declarations: [HelloComponent],
  imports: [BrowserModule, ReactiveFormsModule],
  entryComponents: [HelloComponent]
})
export class AppModule {
  constructor(private injector: Injector) {
    const el = createCustomElement(HelloComponent, { injector });
    customElements.define('hello-world', el);
  }

  ngDoBootstrap() {}
}
