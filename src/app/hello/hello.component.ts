import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {
  FormGroup,
  FormControl,
  FormBuilder,
  Validators
} from '@angular/forms';
@Component({
  selector: 'app-hello',
  templateUrl: './hello.component.html',
  styleUrls: ['./hello.component.css']
})
export class HelloComponent implements OnInit {
  @Input()
  name: string;
  @Output()
  helloClick: EventEmitter<string> = new EventEmitter<string>();
  @Output()
  EmployeeFormSubmit: EventEmitter<{
    firstname: string;
    lastname: string;
  }> = new EventEmitter<{ firstname: string; lastname: string }>();

  get firstName(): FormControl {
    return this.employeeForm.get('firstname') as FormControl;
  }

  get lastName(): FormControl {
    return this.employeeForm.get('lastname') as FormControl;
  }

  employeeForm: FormGroup;
  constructor(private fb: FormBuilder) {}
  ngOnInit() {
    this.employeeForm = this.fb.group({
      firstname: ['', Validators.required],
      lastname: ['', Validators.required]
    });
  }
  onClick() {
    this.helloClick.emit('Hello world component clicked');
  }

  onEmployeeFormSubmit() {
    this.EmployeeFormSubmit.emit(this.employeeForm.value);
    this.employeeForm.reset();
  }
}
